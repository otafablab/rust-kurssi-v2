(require 'org)

(setq org-latex-listings 'minted
      org-latex-compiler "xelatex" ; fontspec support
      org-latex-packages-alist '(("" "minted"))
      org-latex-pdf-process '("latexmk -pdflatex='%latex -8bit -interaction=nonstopmode -shell-escape' -pdf -bibtex -f -output-directory=%o %f")
      org-src-preserve-indentation nil
      org-edit-src-content-indentation 0)

;; Used for getting Latex output buffer from the command line
(defun buffer-whole-string (buffer)
  (with-current-buffer buffer
    (save-restriction
      (widen)
      (buffer-substring-no-properties (point-min) (point-max)))))

(defun print-last-buffer ()
  (print (buffer-whole-string (car (last (buffer-list))))))

;; Generates the final presentation using org-beamer-export-to-pdf and
;; handles errors by printing the last buffer which is assumed to be
;; *Org PDF LaTeX Output*
(defun generate-beamer-presentation ()
  (condition-case err
      (progn
        (org-beamer-export-to-pdf)
        (print-last-buffer))
    (error
     (progn
       (print-last-buffer)
       (kill-emacs 1)))))
