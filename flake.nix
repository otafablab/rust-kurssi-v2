{
  description = "LaTeX Document Demo";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.05";
    flake-utils.url = "github:numtide/flake-utils";
    devenv.url = "github:cachix/devenv";
  };

  nixConfig = {
    extra-trusted-public-keys = "devenv.cachix.org-1:w1cLUi8dv3hnoSPGAuibQv+f9TZLr6cv/Hm9XgU50cw=";
    extra-substituters = "https://devenv.cachix.org";
  };

  outputs = { self, nixpkgs, flake-utils, devenv }@inputs:
    with flake-utils.lib; eachSystem allSystems (system:
      let
        pkgs = import nixpkgs { inherit system; };
        tex = pkgs.texlive.combine {
          inherit (pkgs.texlive)
            scheme-small
            beamertheme-metropolis
            wrapfig
            capt-of
            pgfopts
            hyperref
            minted
            fontspec
            latex-bin
            latexmk;
        };
        extraFonts = pkgs.nerdfonts.override { fonts = [ "ComicShannsMono" ]; };
        customdracula = pkgs.python311Packages.buildPythonPackage {
          pname = "customdracula";
          version = "0.1.0";
          src = ./customdracula;
          doCheck = false;
          propagatedBuildInputs = [
            pkgs.python311Packages.pygments
          ];
        };
        buildInputs = [
          pkgs.emacs-nox
          pkgs.python311Packages.pygments
          tex
          customdracula
        ];
        # Found this gem here https://discourse.nixos.org/t/project-local-fonts/22174
        FONTCONFIG_FILE = pkgs.makeFontsConf {
          fontDirectories = [ pkgs.lmodern extraFonts ];
        };
      in rec {
        devShells.default = devenv.lib.mkShell {
          inherit inputs pkgs;
          modules = [
            ({ pkgs, config, ... }: {
              packages = buildInputs;

              languages.python.enable = true;
              languages.python.venv.enable = true;
              languages.python.venv.requirements = ''
                ./customdracula
                pygments
              '';

              env.GREET = "rust-kurssi-v2";
              env.FONTCONFIG_FILE = FONTCONFIG_FILE;
            })
          ];
        };
        
        packages = {
          devenv-up = devShells.default.config.procfileScript;
          document = pkgs.stdenvNoCC.mkDerivation rec {
            inherit buildInputs FONTCONFIG_FILE;
            name = "org-beamer-export-to-pdf";
            src = self;
            phases = ["unpackPhase" "buildPhase" "installPhase"];
            PATH = pkgs.lib.makeBinPath buildInputs;
            SOURCE_DATE_EPOCH = toString self.lastModified;
            buildPhase = ''
              for orgfile in diat/*.org; do
                echo "Building $orgfile"
                emacs --batch --load init.el "$orgfile" --funcall generate-beamer-presentation
              done
            '';
            installPhase = ''
              mkdir -p $out
              cp diat/*.pdf $out/
            '';
          };
        };
        defaultPackage = packages.document;
      });
}
