from setuptools import setup

setup(
    name="customdracula",
    install_requires=["pygments"],
    packages=["style"],
    entry_points="""
    [pygments.styles]
    customdracula = style:CustomDraculaStyle
    """,
)
