# Konekieli ja debuggaus

Klikkaa alla olevasta painonapista avataksesi luennon diat.

[Diat :material-file-pdf-box:](https://otafablab.gitlab.io/rust-kurssi-v2/03-konekieli-ja-debuggaus.pdf){ .md-button }

## Python debuggaus

```python
def fib(n):
    if n <= 1:
        return n
    return fib(n-2) + fib(n-1)

print(fib(5))
```

```python
def calculator():
    sum = 0
    for n in input().split('+'):
        sum += int(n.strip())
    return sum

print(calculator())
```

## fn one

```rust
pub fn one() -> i32 {
    1
}
```

```asm
example::one:
        mov     eax, 1
        ret
```

## fn fib

```rust
pub fn fib(n: i64) -> i64 {
    if n <= 1 { n }
    else { fib(n.wrapping_sub(1)).wrapping_add(fib(n.wrapping_sub(2))) }
}
```

```asm
fib:
        sub     rsp, 24
        mov     [rsp + 8], rdi
        cmp     rdi, 1
        jle     .if

.else:
        mov     rdi, [rsp + 8]
        sub     rdi, 1
        call    fib
        mov     rdi, [rsp + 8]
        mov     [rsp], rax
        sub     rdi, 2
        call    fib
        mov     rcx, rax
        mov     rax, [rsp]
        add     rax, rcx
        mov     [rsp + 16], rax
        jmp     .end

.if:
        mov     rax, [rsp + 8]
        mov     [rsp + 16], rax

.end:
        mov     rax, [rsp + 16]
        add     rsp, 24
        ret
```
