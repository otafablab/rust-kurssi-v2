# Rajapinnat ja geneeriset tyypit

Klikkaa alla olevasta painonapista avataksesi luennon diat.

[Diat :material-file-pdf-box:](https://otafablab.gitlab.io/rust-kurssi-v2/02-rajapinnat-ja-geneeriset-tyypit.pdf){ .md-button }

## Diojen koodiesimerkit

### `fn upper`

- vaatii `thiserror` kirjaston

```rust
use thiserror::Error;

#[derive(Error, Debug)]
pub enum UpperError {
    #[error("no name provided")]
    NoName,
}

pub fn upper<T>(name: T) -> Result<String, UpperError>
where
    T: AsRef<str>,
{
    let name = name.as_ref(); // convert to &str
    if name.is_empty() {
        return Err(UpperError::NoName);
    }
    Ok(name.to_uppercase())
}

fn main() -> Result<(), UpperError> {
    let name = upper("sammy")?;
    println!("Uppercase name: {}", name);

    match upper("") {
        Ok(_) => unreachable!(),
        Err(e) => println!("failed to upper: {e}"),
    }
    Ok(())
}
```
