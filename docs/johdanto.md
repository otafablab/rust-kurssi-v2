# Johdanto

Klikkaa alla olevasta painonapista avataksesi luennon diat.

[Diat :material-file-pdf-box:](https://otafablab.gitlab.io/rust-kurssi-v2/johdanto.pdf){ .md-button }

```rust
fn start_course() {
    println!("Course starting countdown:");
    for i in (1..=3).rev() {
        println!("{}", i);
    }
}

start_course();
```

## Rustin asennus (Windows)

1. Lataa asennusohjelma [täältä](https://www.rust-lang.org/tools/install) ja suorita se.
2. Valitse `2) Customize installation`.
3. Kirjoita komentoriviin `x86_64-pc-windows-gnu`.
4. Ohita seuraavat kysymykset painamalla enter-näppäintä kunnes olet taas kohdan 2. valikossa.
5. Valitse `1) Proceed with installation (default)`.
